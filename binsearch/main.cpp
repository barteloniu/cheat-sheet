#include<bits/stdc++.h>

int n;
int *a;

int lbs(int query){
	int left = 0, right=n-1, middle;
	while(left < right){
		middle = (left + right) / 2;
		if(query <= a[middle])
			right = middle;
		else
			left = middle + 1;
	}
	return left;
}

int rbs(int query){
	int left = 0, right=n-1, middle;
	while(left < right){
		middle = (left + right + 1) / 2;
		if(query >= a[middle])
			left = middle;
		else
			right = middle - 1;
	}
	return left;
}

int after(int query){
	int left = 0, right=n-1, middle;
	while(left < right){
		middle = (left + right) / 2;
		if(query < a[middle])
			right = middle;
		else
			left = middle + 1;
	}
	return left;
}

int before(int query){
	int left = 0, right=n-1, middle;
	while(left < right){
		middle = (left + right + 1) / 2;
		if(query > a[middle])
			left = middle;
		else
			right = middle - 1;
	}
	return left;
}

int main(){
	std::ios_base::sync_with_stdio(0);
	std::cin.tie(0);
	std::cout.tie(0);

	std::cout << "Hello, World!\n";

	return 0;
}

